/**
*    @class		Zterrain
*    @author	Nigel Landwehr Johann
*    @date		12/3/2016
*    @brief		Class to generate the default terrain of our world.
*
*    @section Description
*
*/

#include "Zterrain.h"
#include "ZsoundSystemClass.h"
#include "Zmain.h"

Zterrain::Zterrain(Ogre::SceneManager *sm, Ogre::TerrainGroup *tg, Ogre::RenderWindow *rw)
{
	zSceneManager = sm;
	zTerrainGroup = tg;
	zWindow = rw;

	// set initial fog level
	Zmain::getZmain()->zWindow->getViewport(0)->setBackgroundColour(backgroundColour);
	Zmain::getZmain()->zSceneManager->setFog(Ogre::FOG_EXP, backgroundColour, 0.0005f);

}

/// Loads the terrain into the TerrainGroup, and sets a SkyBox
void Zterrain::createTerrain()
{
	// Holds all the terrain information
	zTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();

	// Sets how the terrain will be saved
	zTerrainGroup->setFilenameConvention(Ogre::String("bterrain"), Ogre::String("dat"));
	// Set origin to be used for our terrain
	zTerrainGroup->setOrigin(Ogre::Vector3::ZERO);

	configureTerrainDefaults();

	// Define all the terrains
	for (long x = 0; x <= 0; ++x)
		for (long y = 0; y <= 0; ++y)
			defineTerrain(x, y);
	// Loads all the terrains
	zTerrainGroup->loadAllTerrains(true);

	// Loop through any terrain elements and init their blends
	if (zTerrainsImported)
	{
		Ogre::TerrainGroup::TerrainIterator ti = zTerrainGroup->getTerrainIterator();

		while (ti.hasMoreElements())
		{
			Ogre::Terrain* t = ti.getNext()->instance;
			initBlendMaps(t);
		}
	}

	// Cleanup temp resources created while configuring terrain
	zTerrainGroup->freeTemporaryResources();

	// Set Skybox
	zSceneManager->setSkyBox(true, "Examples/SpaceSkyBox");
}
/// Loads the terrain resource
void getTerrainImage(bool flipX, bool flipY, Ogre::Image& img)
{
	img.load("terrain.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
}
/// Used to set up the grid location
void Zterrain::defineTerrain(long x, long y)
{
	// Define filename for this Terrain
	Ogre::String filename = zTerrainGroup->generateFilename(x, y);
	// Checks if a filename for this grid location has already been generated
	bool exist = Ogre::ResourceGroupManager::getSingleton().resourceExists(zTerrainGroup->getResourceGroup(), filename);

	// When it already exist call defineTerrain with the already created filename
	if (exist)
		zTerrainGroup->defineTerrain(x, y);
	// Otherwise generate image with getTerrainImage, call different overload of defineTerrain that takes a reference of our created image
	else {
		Ogre::Image img;
		getTerrainImage(x % 2 != 0, y % 2 != 0, img);
		zTerrainGroup->defineTerrain(x, y, &img);
		// Finally set imported to true
		zTerrainsImported = true;
	}
}
/// Blends the different layers
void Zterrain::initBlendMaps(Ogre::Terrain* terrain)
{
	Ogre::Real minHeight0 = 70;
	Ogre::Real fadeDist0 = 40;
	Ogre::Real minHeight1 = 70;
	Ogre::Real fadeDist1 = 15;

	Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
	Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);

	float* pBlend0 = blendMap0->getBlendPointer();
	float* pBlend1 = blendMap1->getBlendPointer();

	for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
	{
		for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
		{
			Ogre::Real tx, ty;

			blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
			Ogre::Real height = terrain->getHeightAtTerrainPosition(tx, ty);
			Ogre::Real val = (height - minHeight0) / fadeDist0;
			val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
			*pBlend0++ = val;

			val = (height - minHeight1) / fadeDist1;
			val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
			*pBlend1++ = val;
		}
	}

	blendMap0->dirty();
	blendMap1->dirty();
	blendMap0->update();
	blendMap1->update();
}
/// Holds the configuration of the terrain
void Zterrain::configureTerrainDefaults() 
{
	zTerrainGlobals->setMaxPixelError(8);
	zTerrainGlobals->setCompositeMapDistance(3000);

	Ogre::Terrain::ImportData& importData = zTerrainGroup->getDefaultImportSettings();
	importData.terrainSize = 513;
	importData.worldSize = 12000.0;
	importData.inputScale = 600;
	importData.minBatchSize = 33;
	importData.maxBatchSize = 65;

	importData.layerList.resize(3);

	importData.layerList[0].worldSize = 100;
	importData.layerList[0].textureNames.push_back(
		"dirt_grayrocky_diffusespecular.dds");
	importData.layerList[0].textureNames.push_back(
		"dirt_grayrocky_normalheight.dds");
	importData.layerList[1].worldSize = 30;
	importData.layerList[1].textureNames.push_back(
		"grass_green-01_diffusespecular.dds");
	importData.layerList[1].textureNames.push_back(
		"grass_green-01_normalheight.dds");
	importData.layerList[2].worldSize = 200;
	importData.layerList[2].textureNames.push_back(
		"growth_weirdfungus-03_diffusespecular.dds");
	importData.layerList[2].textureNames.push_back(
		"growth_weirdfungus-03_normalheight.dds");
}

Zterrain::~Zterrain()
{
}
