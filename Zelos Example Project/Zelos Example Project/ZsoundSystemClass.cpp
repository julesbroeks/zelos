#include "ZsoundSystemClass.h"


ZsoundSystemClass::ZsoundSystemClass()
{
	if (FMOD::System_Create(&m_pSystem) != FMOD_OK)
	{
		// Report Error
		return;
	}

	int driverCount = 0;
	m_pSystem->getNumDrivers(&driverCount);

	if (driverCount == 0)
	{
		// Report Error
		return;
	}

	// Initialize our Instance with 36 Channels
	m_pSystem->init(36, FMOD_INIT_NORMAL, nullptr);

}

void ZsoundSystemClass::createSound(SoundClass *pSound, const char* pFile)
{
	m_pSystem->createSound(pFile, FMOD_HARDWARE, 0, pSound);
}

void ZsoundSystemClass::playSound(SoundClass pSound, bool bLoop)
{
	if (!bLoop)
		pSound->setMode(FMOD_LOOP_OFF);
	else
	{
		pSound->setMode(FMOD_LOOP_NORMAL);
		pSound->setLoopCount(-1);
	}

	m_pSystem->playSound(FMOD_CHANNEL_FREE, pSound, false, 0);
}

void ZsoundSystemClass::releaseSound(SoundClass pSound)
{
	m_pSystem->playSound(FMOD_CHANNEL_FREE, pSound, true, 0);

}