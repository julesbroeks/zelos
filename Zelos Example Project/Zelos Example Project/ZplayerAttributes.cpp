/**
*    @class		ZplayerAttributes
*    @author	Daan Levendig
*    @date		3/5/2016
*    @brief		Player Attributes
*
*    @section Description
*
*/

using namespace std;

#include "ZplayerAttributes.h"
#include "ZlevelManager.h"
#include "Zlevel.h"
#include "Zmain.h"
#include "fmod.h"
#include "ZsoundSystemClass.h"

ZplayerAttributes* ZplayerAttributes::ZplayerAttributesInstance = nullptr;

// SINGLETON for ZplayerAttributes
ZplayerAttributes* ZplayerAttributes::getZplayerAttributes()
{
	if (ZplayerAttributesInstance == nullptr)
	{
		// error
	}
	return ZplayerAttributesInstance;
}

ZplayerAttributes::ZplayerAttributes()
{
	ZplayerAttributesInstance = this;

	// Create background material
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("POVAstro", "General");
	material->getTechnique(0)->getPass(0)->createTextureUnitState("helmet2.png");
	material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
	material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
	material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
	material->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SceneBlendType::SBT_TRANSPARENT_ALPHA);

	// Create background rectangle covering the whole screen
	rect = new Ogre::Rectangle2D(true);
	rect->setCorners(-1.0, 1.0, 1.0, -1.0);
	rect->setMaterial("POVAstro");

	// Use identity view/projection matrices
	rect->setUseIdentityProjection(true);
	rect->setUseIdentityView(true);

	// Use infinite AAB to always stay visible
	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	rect->setBoundingBox(aabInf);

	// Render just before overlays
	rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY - 2);

	// Attach to scene
	Zmain::getZmain()->zSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(rect);
}

ZplayerAttributes::~ZplayerAttributes()
{
}

void ZplayerAttributes::update()
{
	if (ZlevelManager::getZlevelManager()->zOxygenInt < 0)
	{
		// player runs out of oxygen
		noOxygen = true;
	}
	if (ZlevelManager::getZlevelManager()->zOxygenInt > 100)
	{
		// make sure oxygenlevel wil not be above 100%
		ZlevelManager::getZlevelManager()->zOxygenInt = 100;
	}
	else if (noOxygen)
	{
		// play lastbreath sound when player runs out of oxygen
		ZlevelManager::getZlevelManager()->zOxygenInt = 0;
		Zmain::getZmain()->zSoundManager->releaseSound(choking);
		Zmain::getZmain()->zSoundManager->createSound(&lastBreath, "\\lastbreath.wav");
		Zmain::getZmain()->zSoundManager->playSound(lastBreath, false);
	}
	else
	{
		if (ZlevelManager::getZlevelManager()->zOxygenInt > 20 && !startedBreathing)
		{
			if (startedChoking)
			{
				// stop choking sound
				Zmain::getZmain()->zSoundManager->releaseSound(choking);
				startedChoking = false;
			}
			// play sound breathing
			Zmain::getZmain()->zSoundManager->createSound(&breathing, "\\breathing.wav");
			Zmain::getZmain()->zSoundManager->playSound(breathing, true);
			startedBreathing = true;

		}
		else if (ZlevelManager::getZlevelManager()->zOxygenInt <= 20 && !startedChoking)
		{
			// play sound choking when oxygenlevel is below 20%
			if (startedBreathing)
			{
				Zmain::getZmain()->zSoundManager->releaseSound(breathing);
				startedBreathing = false;
			}
			Zmain::getZmain()->zSoundManager->createSound(&choking, "\\choking.wav");
			Zmain::getZmain()->zSoundManager->playSound(choking, true);

			startedChoking = true;
		}

	}

	// new clock
	currentTime = clock();

	clock_t differenceInTicks = currentTime - lastOxygenUpdate;
	clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
	if (differenceInMs >= 2000 && !noOxygen)
	{
		lastOxygenUpdate = currentTime;
		ZlevelManager::getZlevelManager()->zOxygenInt--;
	}
	else if (differenceInMs >= 1000 && noOxygen)
	{
		lastOxygenUpdate = currentTime;
		ZlevelManager::getZlevelManager()->zHealthInt--;
	}

	ZlevelManager::getZlevelManager()->currentLevel->createHealthPanel("Health: " + to_string(ZlevelManager::getZlevelManager()->zHealthInt));
	ZlevelManager::getZlevelManager()->currentLevel->createOxygenPanel("Oxygen: " + to_string(ZlevelManager::getZlevelManager()->zOxygenInt));
}
