/**
*    @class		Ztimer
*    @author	Luc Schuttel
*    @date		9/3/2016
*    @brief		Timer class to create a countdown timer.
*
*    @section Description
*
*/

#include "Ztimer.h"


//---------------------------------------------------------------------------

Ztimer::Ztimer()
{
	countDown = 0;
};

Ztimer::Ztimer(int durationInMinutes)
{
	countDown = durationInMinutes * 60 * 1000;	// 1 minute -> 60 seconds, 1 second -> 1000 milliseconds
	theTimer = new Ogre::Timer();
}

/// Deconstructor 
Ztimer::~Ztimer(void)
{

};

bool Ztimer::update()
{
	Ogre::Real deltaTime = theTimer->getMilliseconds(); //Get time passed since last update
	theTimer->reset();	//So that deltaTime doesn't count double (for 2+ frames)
	countDown -= deltaTime;
	if (countDown < 0)
	{
		return true;
	}
	return false;
};

void Ztimer::addMinutes(int minutes)
{
	countDown += minutes * 60 * 1000;
}

string Ztimer::getValue()
{
	int totalSeconds = trunc(countDown / 1000);
	int minute = (totalSeconds / 60) % 60;
	int second = totalSeconds % 60;
	return to_string(minute) + ":" + to_string(second);
}