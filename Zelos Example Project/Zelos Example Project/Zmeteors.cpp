#include "Zmeteors.h"
#include "OgreMeshManager.h"
#include "Zmain.h"
#include "Zrigidbody.h"
#include "ZsoundSystemClass.h"
#include "ZlevelManager.h"

Zmeteors::Zmeteors()
{
	meteorList = new list<Zrigidbody*>();

	for (int i = 0; i < 50; i++)
	{
		Zrigidbody *meteor = new Zrigidbody("zMeteorA.mesh", false);
		meteor->zObjectNode->setScale(40, 40, 40);
		// random spawn position in the air
		float xSpawn = Zmain::getZmain()->zSceneManager->getSceneNode("PlayerNode")->getPosition().x + ((float(rand()) / float(RAND_MAX)) * (4000 - -4000)) + -4000;
		float ySpawn = Zmain::getZmain()->zSceneManager->getSceneNode("PlayerNode")->getPosition().y + ((float(rand()) / float(RAND_MAX)) * (20000 - 10000)) + 10000;
		float zSpawn = Zmain::getZmain()->zSceneManager->getSceneNode("PlayerNode")->getPosition().z + ((float(rand()) / float(RAND_MAX)) * (4000 - -4000)) + -4000;



		meteor->zObjectNode->setPosition(Ogre::Vector3(xSpawn, ySpawn, zSpawn));
		meteor->rbody.position = Ogre::Vector3(xSpawn, ySpawn, zSpawn);

		// random downwards velocity
		float xVel = ((float(rand()) / float(RAND_MAX)) * (500 - -500)) + -500;
		float yVel = ((float(rand()) / float(RAND_MAX)) * (0 - -500)) + -500;
		float zVel = ((float(rand()) / float(RAND_MAX)) * (500 - -500)) + -500;

		meteor->rbody.velocity = Ogre::Vector3(xVel, yVel, zVel);

		// random rotation
		float rx = ((float(rand()) / float(RAND_MAX)) * (5 - -5)) + -5;
		float ry = ((float(rand()) / float(RAND_MAX)) * (5 - -5)) + -5;
		float rz = ((float(rand()) / float(RAND_MAX)) * (5 - -5)) + -5;


		meteor->rbody.angularVelocity = Ogre::Vector3(rx, ry, rz);
		meteor->rbody.orientation = meteor->zObjectNode->getOrientation();
		meteor->killPlayerOnTouch = true;

		meteorList->push_front(meteor);
	}

}


Zmeteors::~Zmeteors()
{

}



void Zmeteors::startEvent()
{
	createParamsPanel("WARNING : Meteors incoming!!!");
	resetMeteors();
	playSound();
}

void Zmeteors::playSound()
{
	zSoundManager->createSound(&meteorSound, "\\narrator\\MeteorIncoming.wav");
	zSoundManager->playSound(meteorSound, false);
}

void Zmeteors::stopSound()
{
	zSoundManager->releaseSound(meteorSound);
}

void Zmeteors::createParamsPanel(string panelText)
{
	Ogre::StringVector objectTextItem;
	objectTextItem.push_back(panelText);

	if (Zmain::getZmain()->zTrayManager->getWidget("ObstacleMeteorPanel"))
	{
		Zmain::getZmain()->zTrayManager->destroyWidget("ObstacleMeteorPanel");
	}

	ZlevelManager::getZlevelManager()->ObstacleMeteorPanel = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_RIGHT, "ObstacleMeteorPanel", 300, objectTextItem);
	ZlevelManager::getZlevelManager()->ObstacleMeteorPanel->show();
	
}

void Zmeteors::hideParamsPanel()
{

	ZlevelManager::getZlevelManager()->ObstacleMeteorPanel->hide();
	Zmain::getZmain()->zTrayManager->removeWidgetFromTray(ZlevelManager::getZlevelManager()->ObstacleMeteorPanel);
}

void Zmeteors::resetMeteors()
{
	timeLastShow = clock();
	playSound();

		for (int i = 0; i < 50; i++)
		{
		
			meteorList->front()->isKinematic = false;
			float xSpawn = Zmain::getZmain()->zSceneManager->getSceneNode("PlayerNode")->getPosition().x + ((float(rand()) / float(RAND_MAX)) * (3000 - -3000)) + -3000;
			float ySpawn = Zmain::getZmain()->zSceneManager->getSceneNode("PlayerNode")->getPosition().y + ((float(rand()) / float(RAND_MAX)) * (20000 - 10000)) + 10000;
			float zSpawn = Zmain::getZmain()->zSceneManager->getSceneNode("PlayerNode")->getPosition().z + ((float(rand()) / float(RAND_MAX)) * (3000 - -3000)) + -3000;


			// random downwards velocity
			float xVel = ((float(rand()) / float(RAND_MAX)) * (500 - -500)) + -500;
			float yVel = ((float(rand()) / float(RAND_MAX)) * (0 - -500)) + -500;
			float zVel = ((float(rand()) / float(RAND_MAX)) * (500 - -500)) + -500;

			meteorList->front()->rbody.velocity = Ogre::Vector3(xVel, yVel, zVel);

			// random rotation
			float rx = ((float(rand()) / float(RAND_MAX)) * (5 - -5)) + -5;
			float ry = ((float(rand()) / float(RAND_MAX)) * (5 - -5)) + -5;
			float rz = ((float(rand()) / float(RAND_MAX)) * (5 - -5)) + -5;


			meteorList->front()->rbody.angularVelocity = Ogre::Vector3(rx, ry, rz);
			meteorList->front()->rbody.orientation = meteorList->front()->zObjectNode->getOrientation();
	


			meteorList->front()->zObjectNode->setPosition(Ogre::Vector3(xSpawn, ySpawn, zSpawn));
			meteorList->front()->rbody.position = Ogre::Vector3(xSpawn, ySpawn, zSpawn);


			meteorList->push_back(meteorList->front());
			meteorList->pop_front();
		}

	
		

}
void Zmeteors::update()
{

	clock_t currentTime = clock();

	clock_t differenceInTicks = currentTime - timeLastShow;
	clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
	if (differenceInMs >= 2000 )
	{
		hideParamsPanel();
	}

	//if (differenceInMs >= 3000)
	//{
	//	stopSound();
	//}

	for (int i = 0; i < 50; i++)
	{

		meteorList->front()->update();
		meteorList->push_back(meteorList->front());
		meteorList->pop_front();

	}


}
