#pragma once

#ifndef _ZobstacleSandstorm
#define _ZobstacleSandstorm
#include "ZobstacleEvent.h"
#include <OGRE/OgreSceneNode.h>
#include <time.h>
#include "ZsoundSystemClass.h"

using namespace std;

class ZobstacleSandstorm : public ZobstacleEvent
{



public:

	~ZobstacleSandstorm(void);
	ZobstacleSandstorm();

	void stopEvent();
	void playSound();
	void update();
	void stopSound();
	void createParamsPanel(string panelText);
	void hideParamsPanel();

	void startEvent();

private:

	float maxStormPower = 0.004f;
	float minStormPower = 0.0005f;

	float stormPower = minStormPower;
	Ogre::ColourValue fadeColour = Ogre::ColourValue(0.95f, 0.8f, 0.509804f);

	bool stormActive = false;
	bool isIncreasing = true;
	clock_t lastUpdate = clock();

	ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
	SoundClass stormSound;



};
#endif
