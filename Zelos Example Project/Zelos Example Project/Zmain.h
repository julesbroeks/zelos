#pragma once

using namespace std;

#include "BaseApplication.h"
#include "Zterrain.h"
#include <time.h>
#include "ZsoundSystemClass.h"



//---------------------------------------------------------------------------

class Zmain
{
public:

	static Zmain* getZmain();
	

	~Zmain(void);
	Zmain(Ogre::SceneManager *sceneManager, Ogre::Root *root, Ogre::Camera *camera, Ogre::RenderWindow *rw, OgreBites::SdkTrayManager *tm);

	void createAllObjects();
	void updateAllObjects();
	//void UpdateLoopTimer();
	//void Zmain::Zupdate();
	void ZkeyPressed(const OIS::KeyEvent &arg);
	void ZkeyReleased(const OIS::KeyEvent &arg);
	void ZmouseMoved(const OIS::MouseEvent &arg);
	void UpdateLoopTimer();
	void ZframeRenderQueued();
	
	clock_t timeSinceLastUpdate;
	class Zplayer *player;
	class Zcamera *zCamera;
	class Zplayer *playerTwo;

	class Zprops *lander;
	class Zprops *dish;
	class Zprops *flag;
	class Zprops *habitat;
	class Zprops *solarpanels;

	class Zterrain *zTerrain;

	class ZlevelManager* zLevelManager;

	class ZplayerAttributes* zPlayerAttributes;

	string zEntityname;
	Ogre::SceneManager *zSceneManager;
	Ogre::TerrainGroup* zTerrainGroup;
	OgreBites::SdkTrayManager* zTrayManager;
	Ogre::Root *zRoot;
	Ogre::RenderWindow* zWindow;
	Ogre::Camera *zCameraObj;


	ZsoundSystemClass *zSoundManager;


	OgreBites::ParamsPanel* zPickupPanel;
	
	bool gameStarded = false;
	bool gameEnded = false;
	bool playerDead = false;
	bool playerInBase = false;
	bool meteorsPaused = false;

protected : 
	Zmain(void);

private:
	

	static Zmain* zMainInstance;
	
};

//---------------------------------------------------------------------------
