#pragma once

#ifndef _ZlevelRocket
#define _ZlevelRocket
#include "Zlevel.h"
#include "Zobject.h"

using namespace std;

class ZlevelRocket : public Zlevel, public Zobject
{



public:
	~ZlevelRocket(void);
	ZlevelRocket();
	
	void startLevel();
	void stopLevel();
	void fireObstacleEvent();
	void createObjectivePanel(string panelText);
	void hideObjectivePanel();
	void update();

	Ogre::SceneNode* zpRocket;
	Ogre::Rectangle2D* rect;
	bool inRocket;

protected:

};
#endif
