#pragma once

#ifndef _ZlevelOxygen
#define _ZlevelOxygen
#include "Zlevel.h"
#include "Zobject.h"

using namespace std;

class ZlevelOxygen : public Zlevel, public Zobject
{



public:
	~ZlevelOxygen(void);
	ZlevelOxygen();
	
	void startLevel();
	void stopLevel();
	void fireObstacleEvent();
	void createObjectivePanel(string panelText);
	void hideObjectivePanel();
	void update();

	Ogre::SceneNode* zpOxygenTank;



private:

	int zOxygenTankFillAmount = 30;
};
#endif
