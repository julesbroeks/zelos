#pragma once

#ifndef _Zcamera
#define _Zcamera


using namespace std;

#include "OgreString.h"
#include "Zobject.h"

class Zcamera : public Zobject
{
public:
	~Zcamera(void);
	Zcamera();
	Zcamera(Ogre::Camera* cam);
	void update();
	Ogre::SceneNode* ZobjectNode;
	Ogre::Camera* zCamera;
	Ogre::Vector3 zCamPos;
	Ogre::Quaternion zCamRot;

	HANDLE handleIn;
	HANDLE handleOut;
	COORD KeyWhere;
	COORD MouseWhere;
	COORD EndWhere;
	INPUT_RECORD recordIn;
	DWORD NumRead;

protected:


};
#endif


