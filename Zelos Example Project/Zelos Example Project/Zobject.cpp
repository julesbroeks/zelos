/**
*    @class		Zobject
*    @author	Jules Broeks
*    @date		18/2/2016
*    @brief		Game object class with functions that can be used for different kind of game objects in inherited classes
*
*    @section Description
*    Right now the this class is only used to create the actual model from a .mesh file.
*/

#include "Zobject.h"

/// Creates an entity and scenenode and return the attatched node.
Ogre::SceneNode* Zobject::ZcreateObject(string entityName)
{
	zSceneManager = Zmain::getZmain()->zSceneManager;
	zTrayManager = Zmain::getZmain()->zTrayManager;
	Ogre::Entity* ogreEntity = zSceneManager->createEntity(entityName);
	ZpObjectNode = zSceneManager->getRootSceneNode()->createChildSceneNode();
	ZpObjectNode->attachObject(ogreEntity);

	return ZpObjectNode;
};

Ogre::SceneNode* Zobject::ZcreateObject(string entityMesh, string entityName, string nodeName)
{
	zSceneManager = Zmain::getZmain()->zSceneManager;
	zTrayManager = Zmain::getZmain()->zTrayManager;
	Ogre::Entity* ogreEntity = zSceneManager->createEntity(entityName, entityMesh);
	ZpObjectNode = zSceneManager->getRootSceneNode()->createChildSceneNode(nodeName);
	ZpObjectNode->attachObject(ogreEntity);

	return ZpObjectNode;
};

/// Creates empty object
Zobject::Zobject() 
{	
};
/// Gets the first entity in this node
Ogre::Entity* Zobject::getEntity(Ogre::SceneNode* node)
{
	return static_cast<Ogre::Entity*>(node->getAttachedObject(0));
}
/// Deconstructor 
Zobject::~Zobject(void)
{

};





