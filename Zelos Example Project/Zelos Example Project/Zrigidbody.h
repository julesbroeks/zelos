#pragma once

#ifndef _Zrigidbody
#define _Zrigidbody
#include "Zlevel.h"


using namespace std;

#include "OgreString.h"
#include "Zobject.h"
#include "ZsoundSystemClass.h"


class Zrigidbody : public Zobject, public Zlevel
{

/// Type RigidBody that contains all information a rigid body has.
typedef struct
{
	Ogre::Vector3 position;
	Ogre::Vector3 velocity;
	Ogre::Quaternion orientation;
	Ogre::Vector3 angularVelocity;
} RigidBody;

public:
	~Zrigidbody(void);
	Zrigidbody();
	Zrigidbody(string str, bool isKinematic);
	Zrigidbody(string mesh, string entityName, string nodeName);
	void update();
	
	Ogre::SceneNode* zObjectNode;

	/// Contains al the information needed to simulate physics
	RigidBody rbody;
	bool killPlayerOnTouch = false;
	bool	isKinematic;

protected:

	Ogre::Quaternion angularVelocityToSpin(Ogre::Quaternion rotation, Ogre::Vector3 angularVelocity);
	void handleTerrainCollision();
	Ogre::Camera* playerCam;
	Ogre::SceneNode* playerNode;

	// for player collision
	Ogre::AxisAlignedBox boudingBox;
	Ogre::Vector3 previousCamPos;

	/// Controls whether physics affects the rigidbody
	
	float	height, width, depth;

	Ogre::Timer _timer;
	float dt = 1.0f / 60.0f;	
	float gravity = 9.81f * 80.0f;

	bool playerWasHit = false;
	bool playerCanBeDamaged = true;
	clock_t timePlayerWasHit;
	float previousFogDensity;
	
};
#endif
