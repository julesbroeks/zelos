/**
*    @class		ZlevelOxygen
*    @author	Thomas Schoutsen
*    @date		10/4/2016
*    @brief		Create the get keycard objective level
*
*    @section Description
*
*/

#include "ZlevelOxygen.h"
#include "ZlevelManager.h"
#include "ZsoundSystemClass.h"


//---------------------------------------------------------------------------

ZlevelOxygen::ZlevelOxygen()
{
	// first spawn the oxygen tank somewhere on the map
	zpOxygenTank = Zobject::ZcreateObject("oxygentank.mesh", "entityOxygen", "nodeOxygen");
	zpOxygenTank->setPosition(Ogre::Vector3(-4000, 100, 5000));
	zpOxygenTank->setScale(Ogre::Vector3(20, 20, 20));
	zpOxygenTank->setVisible(false);
};

/// Deconstructor 
ZlevelOxygen::~ZlevelOxygen(void)
{

};

void ZlevelOxygen::startLevel()
{
	OutputDebugString("*** Level Oxygen Started ***\n");
	
	zpOxygenTank->setVisible(true);

	createObjectivePanel("An oxygen tank was located, you should check it out.");

	ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
	SoundClass findOxygenSound;
	zSoundManager->createSound(&findOxygenSound, "\\narrator\\FindOxygenTank.wav");
	zSoundManager->playSound(findOxygenSound, false);
}

void ZlevelOxygen::stopLevel()
{
	Zlevel::stopLevel();

	OutputDebugString("*** Found Oxygen Tank! ***\n");
	OutputDebugString("*** Level Complete! ***\n");
}

void ZlevelOxygen::fireObstacleEvent()
{
	Zlevel::fireObstacleEvent();
}

void ZlevelOxygen::createObjectivePanel(string panelText)
{
	Zlevel::createObjectivePanel(panelText);
}

void ZlevelOxygen::hideObjectivePanel()
{
	Zlevel::hideObjectivePanel();
}

void ZlevelOxygen::update()
{
	Zlevel::update();

	OutputDebugString("*** Level Oxygen Updating ***\n");

	if (ZlevelManager::getZlevelManager()->zpIndicator->getEntity(ZlevelManager::getZlevelManager()->zpIndicator->zObjectNode)->isVisible())
	{
		ZlevelManager::getZlevelManager()->zpIndicator->updateIndicator(zpOxygenTank->getPosition());
	}

	if (zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(zpOxygenTank->getPosition()) < 150)
	{
		ZlevelManager::getZlevelManager()->zOxygenInt += zOxygenTankFillAmount;
		zpOxygenTank->setVisible(false);
		stopLevel();
	}
}