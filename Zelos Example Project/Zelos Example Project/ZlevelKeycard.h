#pragma once

#ifndef _ZlevelKeycard
#define _ZlevelKeycard
#include "Zlevel.h"
#include "Zobject.h"

using namespace std;

class ZlevelKeycard : public Zlevel, public Zobject
{



public:

	~ZlevelKeycard(void);
	ZlevelKeycard();

	void startLevel();
	void stopLevel();
	void fireObstacleEvent();
	void createObjectivePanel(string panelText);
	void hideObjectivePanel(); 
	void createInventoryPanel(string panelText);
	void hideInventoryPanel();
	void update();

	Ogre::SceneNode* zpKeycard;
	Ogre::Rectangle2D* rect;

	OgreBites::ParamsPanel* zBasePanel;
	

private:
	bool hasKeycard = false;
	Ogre::Overlay* overlay;

	clock_t lastKeycardUpdate;

	int inBase;

};
#endif
