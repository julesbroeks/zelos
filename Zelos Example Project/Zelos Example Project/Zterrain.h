#pragma once
#include <OGRE/Terrain/OgreTerrainGroup.h>
#include <OgreRenderWindow.h>

class Zterrain
{

public:
	Zterrain(Ogre::SceneManager *sm, Ogre::TerrainGroup *tg, Ogre::RenderWindow *rw);
	void createTerrain();
	~Zterrain();

protected:
	Ogre::SceneManager* zSceneManager;
	Ogre::TerrainGroup* zTerrainGroup;
	Ogre::RenderWindow* zWindow;

	void configureTerrainDefaults();
	void defineTerrain(long x, long y);
	void initBlendMaps(Ogre::Terrain* terrain);

	bool zTerrainsImported;
	Ogre::TerrainGlobalOptions* zTerrainGlobals;

private:
	Ogre::ColourValue backgroundColour = Ogre::ColourValue(0.95f, 0.8f, 0.509804f);

};

