/**
*    @class		ZmainApplication
*    @author	Team Zelos
*    @date		16/2/2016
*    @brief		Inherits from BaseApplication and creates the initial scene, update loop and input handler.
*
*    @section Description
*    
*/

#include "ZmainApplication.h"
#include "Zmain.h"
#include "Ztimer.h"

//---------------------------------------------------------------------------
ZmainApplication::ZmainApplication(void)
{
}
//---------------------------------------------------------------------------
ZmainApplication::~ZmainApplication(void)
{
}

//---------------------------------------------------------------------------
void ZmainApplication::createScene(void)
{
	// sets the light !
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
	Ogre::Light* light = mSceneMgr->createLight("MainLight");
	light->setPosition(20, 80, 50);
	
	// creates a main and  calls the create objects method.
	zMain = new Zmain(mSceneMgr, mRoot, mCamera, mWindow, mTrayMgr);
	Zmain::getZmain()->createAllObjects();	
	

	
}

void ZmainApplication::destroyScene()
{
}

void ZmainApplication::createFrameListener(void)
{
	BaseApplication::createFrameListener();
	theTimer = new Ztimer(5);

	Ogre::StringVector timeLeftItem;
	timeLeftItem.push_back("Time Left:");

	zTimerPanel = mTrayMgr->createParamsPanel(OgreBites::TL_TOP, "TimeLeftPanel", 155, timeLeftItem);
	zTimerPanel->show();
}

bool ZmainApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	// updates all objects in main every frame.
	//Zmain::getZmain()->updateAllObjects();
	Zmain::getZmain()->ZframeRenderQueued();

	if (mWindow->isClosed())
		return false;

	if (mShutDown)
		return false;

	// Need to capture/update each device
	mKeyboard->capture();
	mMouse->capture();

	mTrayMgr->frameRenderingQueued(evt);

	if (!mTrayMgr->isDialogVisible())
	{
	//	mCameraMan->frameRenderingQueued(evt);   // If dialog isn't up, then update the camera
		if (mDetailsPanel->isVisible())          // If details panel is visible, then update its contents
		{
			mDetailsPanel->setParamValue(0, Ogre::StringConverter::toString(mCamera->getDerivedPosition().x));
			mDetailsPanel->setParamValue(1, Ogre::StringConverter::toString(mCamera->getDerivedPosition().y));
			mDetailsPanel->setParamValue(2, Ogre::StringConverter::toString(mCamera->getDerivedPosition().z));
			mDetailsPanel->setParamValue(4, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().w));
			mDetailsPanel->setParamValue(5, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().x));
			mDetailsPanel->setParamValue(6, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().y));
			mDetailsPanel->setParamValue(7, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().z));
		}
		if (theTimer->update())	// Update returns true when the timer reaches 0
		{
			Ogre::StringVector myNothing;
			myNothing.push_back("");
			zTimerPanel->setAllParamNames(myNothing);
			zTimerPanel->setParamValue(0, "WINNING!");	// Game end
		}
		else
		{
			zTimerPanel->setParamValue(0, theTimer->getValue());	// Update time UI
		}
	}

	return true;
}

//---------------------------------------------------------------------------
bool ZmainApplication::keyPressed(const OIS::KeyEvent &arg)
{
	BaseApplication::keyPressed(arg);

	if (mTrayMgr->isDialogVisible()) return true;   // don't process any more keys if dialog is up

	
	zMain->ZkeyPressed(arg);
	//mCameraMan->injectKeyDown(arg);
	return true;
}
//---------------------------------------------------------------------------
bool ZmainApplication::keyReleased(const OIS::KeyEvent &arg)
{
	zMain->ZkeyReleased(arg);
	//mCameraMan->injectKeyUp(arg);
	return true;
}
bool ZmainApplication::mouseMoved(const OIS::MouseEvent &arg)
{
	BaseApplication::mouseMoved(arg);

	zMain->ZmouseMoved(arg);

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
		ZmainApplication app;

        try {
            app.go();
			
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
