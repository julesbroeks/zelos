#include "ZobstacleEvent.h"
#include "ZlevelManager.h"


ZobstacleEvent::ZobstacleEvent()
{
}


ZobstacleEvent::~ZobstacleEvent()
{
}

void ZobstacleEvent::playSound()
{
	
}

void ZobstacleEvent::update()
{
	
}

void ZobstacleEvent::startEvent()
{

}

void ZobstacleEvent::stopSound()
{
	
}

void ZobstacleEvent::createParamsPanel(string panelText)
{
	Ogre::StringVector objectTextItem;
	objectTextItem.push_back(panelText);

	if (Zmain::getZmain()->zTrayManager->getWidget("ObstacleEventPanel"))
	{
		Zmain::getZmain()->zTrayManager->destroyWidget("ObstacleEventPanel");
	}

	ZlevelManager::getZlevelManager()->zObjectivePanel = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_RIGHT, "ObstacleEventPanel", 300, objectTextItem);
	ZlevelManager::getZlevelManager()->zObjectivePanel->show();
}

void ZobstacleEvent::hideParamsPanel()
{
	ZlevelManager::getZlevelManager()->zObjectivePanel->hide();
	Zmain::getZmain()->zTrayManager->removeWidgetFromTray(ZlevelManager::getZlevelManager()->zObjectivePanel);
}
