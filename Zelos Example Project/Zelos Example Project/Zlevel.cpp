/**
*    @class		Zlevel
*    @author	Thomas Schoutsen
*    @date		10/4/2016
*    @brief		Create a new level
*
*    @section Description
*
*/

#include "Zlevel.h"
#include "Zmeteors.h"
#include "ZlocationIndicator.h"
#include "Zmain.h"
#include "ZobstacleSandstorm.h"


//---------------------------------------------------------------------------
Zlevel::Zlevel()
{
}

/// Deconstructor 
Zlevel::~Zlevel(void)
{

};

void Zlevel::startLevel()
{
}

void Zlevel::stopLevel()
{

	hideObjectivePanel();
	ZlevelManager::getZlevelManager()->startNextLevel();
}

void Zlevel::fireObstacleEvent()
{
	if (meteorRain == nullptr)
	{
		meteorRain = new Zmeteors();
		
	}
	
	meteorRain->startEvent();

	/*if (sandStorm == nullptr)
	{
		sandStorm = new ZobstacleSandstorm();
		sandStorm->startEvent();
	}*/

	

}

void Zlevel::createObjectivePanel(string panelText)
{
	Ogre::StringVector objectTextItem; 
	objectTextItem.push_back(panelText);

	if (Zmain::getZmain()->zTrayManager->getWidget("ObjectivePanel"))
	{
		Zmain::getZmain()->zTrayManager->destroyWidget("ObjectivePanel");
	}
	
	ZlevelManager::getZlevelManager()->zObjectivePanel = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_BOTTOM, "ObjectivePanel", 790, objectTextItem);
	ZlevelManager::getZlevelManager()->zObjectivePanel->show();
}

void Zlevel::hideObjectivePanel()
{
	ZlevelManager::getZlevelManager()->zObjectivePanel->hide();
	Zmain::getZmain()->zTrayManager->removeWidgetFromTray(ZlevelManager::getZlevelManager()->zObjectivePanel);
}

void Zlevel::createInventoryPanel(string panelText)
{
	Ogre::StringVector objectTextItem;
	objectTextItem.push_back(panelText);

	if (Zmain::getZmain()->zTrayManager->getWidget("InventoryPanel"))
	{
		Zmain::getZmain()->zTrayManager->destroyWidget("InventoryPanel");
	}

	ZlevelManager::getZlevelManager()->zInventoryPanel = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_TOPRIGHT, "InventoryPanel", 90, objectTextItem);
	ZlevelManager::getZlevelManager()->zInventoryPanel->show();
}

void Zlevel::hideInventoryPanel()
{
	ZlevelManager::getZlevelManager()->zInventoryPanel->hide();
	Zmain::getZmain()->zTrayManager->removeWidgetFromTray(ZlevelManager::getZlevelManager()->zInventoryPanel);
}

void Zlevel::createOxygenPanel(string panelText)
{
	Ogre::StringVector objectTextItem;
	objectTextItem.push_back(panelText);

	if (Zmain::getZmain()->zTrayManager->getWidget("OxygenPanel"))
	{
		Zmain::getZmain()->zTrayManager->destroyWidget("OxygenPanel");
	}

	ZlevelManager::getZlevelManager()->zOxygenPanel = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_TOPLEFT, "OxygenPanel", 120, objectTextItem);
	ZlevelManager::getZlevelManager()->zOxygenPanel->show();
}

void Zlevel::hideOxygenPanel()
{
	ZlevelManager::getZlevelManager()->zOxygenPanel->hide();
}

void Zlevel::createHealthPanel(string panelText)
{
	Ogre::StringVector objectTextItem;
	objectTextItem.push_back(panelText);

	if (Zmain::getZmain()->zTrayManager->getWidget("HealthPanel"))
	{
		Zmain::getZmain()->zTrayManager->destroyWidget("HealthPanel");
	}

	ZlevelManager::getZlevelManager()->zHealthPanel = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_TOPLEFT, "HealthPanel", 120, objectTextItem);
	ZlevelManager::getZlevelManager()->zHealthPanel->show();
}

void Zlevel::hideHealthPanel()
{
	ZlevelManager::getZlevelManager()->zHealthPanel->hide();
}

void Zlevel::showDeathOverlay()
{
	// Create background material
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("DeathScreen", "General");
	material->getTechnique(0)->getPass(0)->createTextureUnitState("DeathScreen.jpg");
	material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
	material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
	material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

	// Create background rectangle covering the whole screen
	rect = new Ogre::Rectangle2D(true);
	rect->setCorners(-1.0, 1.0, 1.0, -1.0);
	rect->setMaterial("DeathScreen");

	// Use identity view/projection matrices
	rect->setUseIdentityProjection(true);
	rect->setUseIdentityView(true);

	// Use infinite AAB to always stay visible
	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	rect->setBoundingBox(aabInf);

	// Render just before overlays
	rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY - 1);

	// Attach to scene
	Zmain::getZmain()->zSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(rect);
}

void Zlevel::update()
{
	if (meteorRain != nullptr && Zmain::getZmain()->meteorsPaused == false)
	{
		meteorRain->update();
	}
	
	/*if (sandStorm != nullptr)
	{
		sandStorm->update();
	}*/
}
