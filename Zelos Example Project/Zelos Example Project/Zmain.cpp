/**
*    @class		Zmain
*    @author	Team Zelos
*    @date		16/2/2016
*    @brief		Creates all objects and runs main update loop.
*
*    @section Description
*
*/

#include "Zplayer.h"
#include "Zprops.h"
#include "Zcamera.h"
#include "Zrigidbody.h"
#include "Zmain.h"
#include "BaseApplication.h"
#include <time.h>
#include <atlstr.h>
#include <iostream>
#include <thread>
#include "ZsoundSystemClass.h"
#include "ZlevelManager.h"
#include "ZplayerAttributes.h"

//---------------------------------------------------------------------------
// Constructor -- not to be used. ( use the constructor that sets the scenemanager)
Zmain* Zmain::zMainInstance = 0;

Zmain::Zmain(void)
{

}
// Deconstructor 
Zmain::~Zmain(void)
{

}

// SINGLETON for zmain
Zmain* Zmain::getZmain()
{
	if (zMainInstance == 0)
	{
	 // error ! should never happen
	}
	return zMainInstance;
}

Zmain::Zmain(Ogre::SceneManager *sceneManager, Ogre::Root *root, Ogre::Camera *camera, Ogre::RenderWindow *rw, OgreBites::SdkTrayManager *tm)
{
	timeSinceLastUpdate = clock();

	// SETS THE SINGLETON when created @ ZmainApplication
	zMainInstance = this;
	zSceneManager = sceneManager; //!< used to add objects to the scene
	zRoot = root; //!< not used yet, will be later for various functions
	zCameraObj = camera;
	zWindow = rw;
	zTrayManager = tm;
	//feedback						 

	/* enables the console output  ->  CAREFULL  !!!  INPUT MIGHT NOT WORK AS EXPECTED ( switching windows will THAN only than execute , use witch causion allways disable when pushing to git.
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);  */
}

/// This will be called on initialising of the game --  all premade object are supposed to be put here -- not directly 
/// make new classes that create them and call them here ( Zplayer Player = new  Zplayer() and let THEM create the item
void Zmain::createAllObjects()
{
	// Manages a grid of terrains
	zTerrainGroup = OGRE_NEW Ogre::TerrainGroup( zSceneManager, Ogre::Terrain::ALIGN_X_Z, 513, 12000.0);

	// create the terrain
	zTerrain = new Zterrain(zSceneManager, zTerrainGroup, zWindow);
	zTerrain->createTerrain(); 

	// create the player
	player = new Zplayer("PlayerNode");// can build this further for a stricter init

	// add props
	solarpanels = new Zprops("zSolarpanels.mesh", "solarPanelNode", Ogre::Vector3(2100, -10, 1500), 90, false);
	dish = new Zprops("dish.mesh", "dishNode", Ogre::Vector3(2550, 130, 300), Ogre::Vector3(40.0f, 40.0f, 40.0f), true);
	flag = new Zprops("flag.mesh", "flagNode", Ogre::Vector3(1718, 160, 778), Ogre::Vector3(40.0f, 40.0f, 40.0f), true);
	habitat = new Zprops("zCapsules.mesh","zHabitatNode", Ogre::Vector3(2100, -10, 1500), 90, true);

	// create the levelmanager that handles the order in which the levels will be played
	zLevelManager = new ZlevelManager();

	// create playerattributes HUD
	zPlayerAttributes = new ZplayerAttributes();

	//soundmanager
	zSoundManager = new ZsoundSystemClass();
	SoundClass soundSample;
	zSoundManager->createSound(&soundSample, "\\background.ogg");

	// Play the sound, with loop mode
	zSoundManager->playSound(soundSample, false);


}


 // dont use this for updates ( different thread )  
 // loops the while untill game quits ( main thread aborting will make this abort aswell ) 
 // timesincelastUpdate will be set once.  than in the while the currenttime will be set, looping it untill 20 miliseconds have passed than
 // updating the timesincelastupdate and calling the update method to the mainthread stack.
 void Zmain::UpdateLoopTimer()
 {
	 clock_t timeSinceLastUpdate = clock();
	 while (!gameEnded)
	 {
		 clock_t currentTime = clock();
		 clock_t differenceInTicks = currentTime - timeSinceLastUpdate;
		 clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
		 if (differenceInMs >= 20)
		 {
			 timeSinceLastUpdate = currentTime;
			// this->updateAllObjects();
		 }
	 }

 }

 void Zmain::updateAllObjects()
 {
	 zLevelManager->update();
	 if (!playerInBase) {
		 player->update();
	 }
	 dish->update();
	 flag->update();
	 solarpanels->update();
	 habitat->update();
	 zPlayerAttributes->update();
 }
 
  // gets called when every frame is quee
 void Zmain::ZframeRenderQueued()
 {
	

     clock_t currentTime = clock();
	clock_t differenceInTicks = currentTime - timeSinceLastUpdate;
	clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
		 if (differenceInMs >= 20)
		 {
			 timeSinceLastUpdate = currentTime;
			 if (!playerDead) {
				 this->updateAllObjects();
			 }
		 }
 }


 void Zmain::ZkeyPressed(const OIS::KeyEvent &arg)
 {
	 if (arg.key == OIS::KC_ESCAPE)   
	 {
		 gameEnded = true;
	 }
	 else
	 {
		player->ZkeyPressed(arg);
	 }
 }
 //---------------------------------------------------------------------------
 void Zmain::ZkeyReleased(const OIS::KeyEvent &arg)
 {
	 player->ZkeyReleased(arg);
	 
 }

 void Zmain::ZmouseMoved(const OIS::MouseEvent &arg)
 {
	 player->ZmouseMoved(arg);
 }
 
