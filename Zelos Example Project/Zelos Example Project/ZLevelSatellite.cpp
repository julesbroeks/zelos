/**
*    @class		ZlevelSatellite
*    @author	Nigel Landwehr Johann
*    @date		26/4/2016
*    @brief		Create the get Satellite objective level
*
*    @section Description
*
*/

#include "ZlevelSatellite.h"
#include "ZsoundSystemClass.h"


//---------------------------------------------------------------------------


ZlevelSatellite::ZlevelSatellite()
{
	zSceneManager = Zmain::getZmain()->zSceneManager;
	Ogre::StringVector objectTextItem;
	objectTextItem.push_back("Press E to restore the connection");

	Zmain::getZmain()->zPickupPanel  = Zmain::getZmain()->zTrayManager->createParamsPanel(OgreBites::TL_CENTER, "PickupPanel", 300, objectTextItem);

}; 

/// Deconstructor 
ZlevelSatellite::~ZlevelSatellite(void)
{
};


void ZlevelSatellite::startLevel()
{
	OutputDebugString("*** Level Satellite Started ***\n");

	createObjectivePanel("Find the satellite dish to start the game");
}

void ZlevelSatellite::stopLevel()
{

	Zlevel::stopLevel();

	OutputDebugString("*** Found Satellite! ***\n");
	OutputDebugString("*** Level Complete! ***\n");
}

void ZlevelSatellite::fireObstacleEvent()
{
	Zlevel::fireObstacleEvent();
}

void ZlevelSatellite::createObjectivePanel(string panelText)
{
	Zlevel::createObjectivePanel(panelText);
}

void ZlevelSatellite::hideObjectivePanel()
{
	Zlevel::hideObjectivePanel();
}



/// Is called each frame
void ZlevelSatellite::update()
{
	Zlevel::update();

	//OutputDebugString("*** Level Satellite Updating ***\n");
	float dishDist = zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(zSceneManager->getSceneNode("dishNode")->getPosition());

	// Used to set an delay for starting the next level, otherwise two sounds clips will be played simultaneously
	if (!dishActive) {
		if (dishDist < 400)
		{
			Zmain::getZmain()->zPickupPanel->show();
			Zmain::getZmain()->zTrayManager->moveWidgetToTray(Zmain::getZmain()->zPickupPanel, OgreBites::TL_RIGHT, 0);


				if (GetAsyncKeyState('E') & 0x8000)
				{

				ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
				SoundClass suriveSound;
				zSoundManager->createSound(&suriveSound, "\\narrator\\Survive15Mins.wav");
				zSoundManager->playSound(suriveSound, false);

				Zmain::getZmain()->zPickupPanel->hide();
				Zmain::getZmain()->zTrayManager->removeWidgetFromTray(Zmain::getZmain()->zPickupPanel);

				dishActive = true;
				ZlevelManager::getZlevelManager()->dishActivated = true;
				lastDishUpdate = clock();

				}
		}
		else if (dishDist >= 200 && !Zmain::getZmain()->zTrayManager->isDialogVisible())
		{
		Zmain::getZmain()->zPickupPanel->hide();
		Zmain::getZmain()->zTrayManager->removeWidgetFromTray(Zmain::getZmain()->zPickupPanel);

		}

	} else {
		clock_t currentTime = clock();

		clock_t differenceInTicks = currentTime - lastDishUpdate;
		clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
		if (differenceInMs >= 5000)
		{
			OutputDebugString("*** Delay over, stopping current Level ***\n");
			OutputDebugString("testttttttttttttttttttttt");

			stopLevel();
		}
	}
};