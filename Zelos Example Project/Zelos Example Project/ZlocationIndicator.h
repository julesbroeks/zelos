#pragma once


#ifndef _ZlocationIndicator
#define _ZlocationIndicator

using namespace std;

#include "Zobject.h"

class ZlocationIndicator : public Zobject
{
public:
	ZlocationIndicator();
	ZlocationIndicator(string str);
	~ZlocationIndicator();
	void updateIndicator(Ogre::Vector3 lookAtPosition);
	void showIndicator();
	void hideIndicator();

	Ogre::SceneNode* zObjectNode;

protected:
};

#endif
