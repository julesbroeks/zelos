#pragma once

using namespace std;

#include "Zmain.h"
#include "ZsoundSystemClass.h"
#include <time.h>

class ZplayerAttributes
{
public:
	static ZplayerAttributes* getZplayerAttributes();

	ZplayerAttributes();
	~ZplayerAttributes();

	virtual void update();

	string zOxygenCount;
	SoundClass breathing;
	SoundClass choking;
	SoundClass lastBreath;
	bool startedBreathing = false;
	bool startedChoking = false;
	bool noOxygen = false;

private:

	static ZplayerAttributes* ZplayerAttributesInstance;
	clock_t currentTime;
	clock_t lastOxygenUpdate;
	Ogre::Rectangle2D* rect;
};

