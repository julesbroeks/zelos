#pragma once

#include "ZplayerExample.h"
#include "BaseApplication.h"

ZplayerExample::ZplayerExample(){
	mRoot->addFrameListener(this);
};

ZplayerExample::~ZplayerExample(){
	
};
bool frameStarted(Ogre::FrameEvent &evt)
{
	// called before frame updates
	OutputDebugString("frame started test ! ");
	return true; //if you return false, the program ends !
}

bool frameEnded(Ogre::FrameEvent &evt)
{
	OutputDebugString("frame ended test ! ");
	//	called after frame updates
	return true; //if you return false, the program ends !
}