#pragma once

#include "ZmainApplication.h"
#include "ZlocationIndicator.h"
#include "Zlevel.h"
#include <stack>
#include <time.h>

using namespace std;


class ZlevelManager
{

public:

	static ZlevelManager* getZlevelManager();


	void startNextLevel();
	void update();

	//class ZlocationIndicator* indicator;
	class ZlocationIndicator* zpIndicator;

	ZlevelManager();

	/// Deconstructor 
	~ZlevelManager(void);
	
	class Zlevel* currentLevel;
	OgreBites::ParamsPanel* zObjectivePanel;
	OgreBites::ParamsPanel* zInventoryPanel;
	OgreBites::ParamsPanel* zOxygenPanel;
	OgreBites::ParamsPanel* zHealthPanel;
	OgreBites::ParamsPanel* zObstacleEventPanel;
	OgreBites::ParamsPanel* ObstacleMeteorPanel;

	int zOxygenInt = 100;
	int zHealthInt = 100;
	bool dishActivated;
	bool damageFogIsDisplayed = false;

protected:

	Ogre::SceneManager* zSceneManager;
	OgreBites::SdkTrayManager* zTrayManager;

	class Zlevel* startLevel;
	class Zlevel* level1;
	class Zlevel* level2;
	class Zlevel* finalLevel;
	std::stack<Zlevel*> levelStack;
	

	// has to equal the number of levels we have
	UINT numberOfLevels = 3;
	// will hold level numbers
	vector<UINT> remainingLevels;
	// currently active level
	int activeLevel = -1;

private:

	static ZlevelManager* ZlevelManagerInstance;
	clock_t lastIndicatorUpdate;
	clock_t lastEventUpdate;	

	ZobstacleEvent *sandStorm;
};
