/**
*    @class		ZlevelManager
*    @author	Thomas Schoutsen
*    @date		10/4/2016
*    @brief		Manage all levels
*
*    @section Description
*    
*/

#include "ZlevelManager.h"
#include "ZlevelKeycard.h"
#include "ZlevelOxygen.h"
#include "ZlevelRocket.h"
#include "ZlevelSatellite.h"

//---------------------------------------------------------------------------


ZlevelManager* ZlevelManager::ZlevelManagerInstance = nullptr;

// SINGLETON for ZlevelManager
ZlevelManager* ZlevelManager::getZlevelManager()
{
	if (ZlevelManagerInstance == nullptr)
	{
		// error
	}
	return ZlevelManagerInstance;
}

ZlevelManager::ZlevelManager()
{
	ZlevelManagerInstance = this;

	zSceneManager = Zmain::getZmain()->zSceneManager;
	zTrayManager = Zmain::getZmain()->zTrayManager;

	lastIndicatorUpdate = clock();
	lastEventUpdate = clock();

	// create instances of the different levels
	zpIndicator = new ZlocationIndicator("indicator.mesh");
	startLevel = new ZlevelSatellite();
	level1 = new ZlevelKeycard();
	level2 = new ZlevelOxygen();
	finalLevel = new ZlevelRocket();

	levelStack.push(finalLevel);
	levelStack.push(level2);
	levelStack.push(level1);
	levelStack.push(startLevel);
	currentLevel = levelStack.top();

	// start next level
	startNextLevel();

	dishActivated = false;

	sandStorm = new ZobstacleSandstorm();
	//sandStorm->startEvent();
	
};


/// Deconstructor 
ZlevelManager::~ZlevelManager(void)
{

};


/// Is called each frame
void ZlevelManager::update()
{
	currentLevel->update();

	sandStorm->update();

		// new clock
	clock_t currentTime = clock();

	if (dishActivated){

		clock_t differenceInTicks = currentTime - lastIndicatorUpdate;
		clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);

		if (differenceInMs >= 20000)
		{
			zpIndicator->showIndicator();
			lastIndicatorUpdate = currentTime;
		}
		else if (differenceInMs >= 1500)
		{
			zpIndicator->hideIndicator();
		}
	}

	// spawn events
	clock_t differenceInTicksEvents = currentTime - lastEventUpdate;
	clock_t differenceInMsEvents = differenceInTicksEvents / (CLOCKS_PER_SEC / 1000);
	if (differenceInMsEvents >= 20000)
	{
		currentLevel->fireObstacleEvent();
		lastEventUpdate = currentTime;
	}


};

void ZlevelManager::startNextLevel()
{
	if (!levelStack.empty())
	{
		currentLevel = levelStack.top();
		currentLevel->startLevel();
		levelStack.pop();
	}
	else
	{
	}

	
}