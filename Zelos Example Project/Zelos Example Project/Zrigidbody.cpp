/**    
*    @class		Zrigidbody
*    @author	Thomas Schoutsen
*    @date		12/3/2016
*    @brief		Create a gameobject with rigid body dynamics
*
*    @section Description
*    Right now the rigid body contains only particle physics
*/

#include "Zrigidbody.h"
#include "ZsoundSystemClass.h"

//---------------------------------------------------------------------------
Zrigidbody::Zrigidbody()
{

};

/// Constructor to create a new rigid body object
Zrigidbody::Zrigidbody(string meshName, bool kinematic) 
{

	zObjectNode = Zobject::ZcreateObject(meshName);
	isKinematic = kinematic;

	width = Zobject::getEntity(zObjectNode)->getBoundingBox().getSize().x;
	height = Zobject::getEntity(zObjectNode)->getBoundingBox().getSize().y;
	depth = Zobject::getEntity(zObjectNode)->getBoundingBox().getSize().z;

	playerNode = zSceneManager->getSceneNode("PlayerNode");
	
	zObjectNode->showBoundingBox(false);
};

Zrigidbody::Zrigidbody(string mesh, string entityName, string nodeName)
{
	zObjectNode = Zobject::ZcreateObject(mesh, entityName, nodeName);
}


/// Deconstructor 
Zrigidbody::~Zrigidbody(void)
{

};

/// Is called each frame
void Zrigidbody::update()
{	
	clock_t currentTime = clock();
	clock_t differenceInTicks = currentTime - timePlayerWasHit;

	clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);

	if (differenceInMs >= 1000 && playerWasHit)
	{
		Zmain::getZmain()->zSceneManager->setFog(Ogre::FOG_EXP, Ogre::ColourValue(0.95f, 0.8f, 0.509804f), previousFogDensity);
		playerWasHit = false;
		ZlevelManager::getZlevelManager()->damageFogIsDisplayed = false;
		playerCanBeDamaged = true;
	}

	if (!isKinematic){

		handleTerrainCollision();		
	
		//--------------------------------------------------------------------
		// Update movement

	
			
			// lineair movement
			rbody.velocity += Ogre::Vector3(0.0f, -gravity, 0.0f) * dt;
			rbody.position += rbody.velocity * dt;

			zObjectNode->setPosition(rbody.position);

			// angular movement
			Ogre::Quaternion spin = angularVelocityToSpin(rbody.orientation, rbody.angularVelocity);

			rbody.orientation = rbody.orientation + (spin * dt);
			rbody.orientation.normalise();

			zObjectNode->setOrientation(rbody.orientation);

			// reset time
			_timer.reset();
		
		
	}


	//---------------------------------------------------------
	// Collide with player
	Ogre::AxisAlignedBox aab = zObjectNode->_getWorldAABB().intersection(playerNode->_getWorldAABB());
	


	if (!aab.isNull())
	{
		// flash colour
		//Zmain::getZmain()->zWindow->getViewport(0)->setBackgroundColour(Ogre::ColourValue(0.862745f, 0.0784314f, 0.235294f));
		Zmain::getZmain()->zSceneManager->setFog(Ogre::FOG_EXP, Ogre::ColourValue(0.980392f, 0.501961f, 0.447059f), 0.005f);
		ZlevelManager::getZlevelManager()->damageFogIsDisplayed = true;
		playerWasHit = true;
		previousFogDensity = Zmain::getZmain()->zSceneManager->getFogDensity();
		timePlayerWasHit = clock();

		if (playerCanBeDamaged) {
			ZlevelManager::getZlevelManager()->zHealthInt -= 25;
			

			ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
			SoundClass painSound;
			zSoundManager->createSound(&painSound, "\\pain.wav");
			zSoundManager->playSound(painSound, false);

			playerCanBeDamaged = false;
		}

		if (ZlevelManager::getZlevelManager()->zHealthInt <= 0)
		{
			Zlevel::showDeathOverlay();
			Zmain::getZmain()->playerDead = true;
			Zmain::getZmain()->zTrayManager->hideAll();
		}
		else
		{
			Ogre::Vector3 diff = playerNode->getPosition() - zObjectNode->getPosition();
			Ogre::Vector3 dir = diff.normalisedCopy();
			Ogre::Vector3 penentration = aab.getMaximum() - aab.getMinimum();
			Ogre::Vector3 trans = dir * Ogre::Math::Abs(penentration.normalisedCopy().dotProduct(dir)) * penentration.length() * 0.4f;

			playerNode->setPosition(playerNode->getPosition() + trans);
		}
	}
};

/// Convert a Vector3 angular velocity to a Quaternion
Ogre::Quaternion Zrigidbody::angularVelocityToSpin(Ogre::Quaternion orientation, Ogre::Vector3 angularVelocity)
{
	const float x = angularVelocity.x;
	const float y = angularVelocity.y;
	const float z = angularVelocity.z;

	return 0.5f * Ogre::Quaternion(0, x, y, z) * orientation;
}

/// Check if object collides with the ground
void Zrigidbody::handleTerrainCollision()
{
	Ogre::Vector3 rBodyPos = zObjectNode->getPosition();
	Ogre::Ray rigidBodyRay(
		Ogre::Vector3(rBodyPos.x, 5000.0, rBodyPos.z),
		Ogre::Vector3::NEGATIVE_UNIT_Y);

	Ogre::TerrainGroup::RayResult result = Zmain::getZmain()->zTerrainGroup->rayIntersects(rigidBodyRay);

	if (result.terrain) {
		Ogre::Real terrainHeight = result.position.y;
		// When the rigidbody object is below the terrain, place on the terrain
		if (rBodyPos.y - height / 2 < terrainHeight) {
			zObjectNode->setPosition(Ogre::Vector3(zObjectNode->getPosition().x, terrainHeight + height / 2, zObjectNode->getPosition().z));
			isKinematic = true;
			killPlayerOnTouch = false;
		}
	}
}