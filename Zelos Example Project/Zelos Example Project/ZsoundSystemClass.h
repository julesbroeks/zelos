#pragma once


#include "fmod.hpp"


typedef FMOD::Sound* SoundClass;

class ZsoundSystemClass
{
	// Pointer to the FMOD instance
public :
	
	FMOD::System *m_pSystem;
	ZsoundSystemClass();
	void createSound(SoundClass *pSound, const char* pFile);
	void playSound(SoundClass pSound, bool bLoop = false);
	void releaseSound(SoundClass pSound);
};