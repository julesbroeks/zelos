#pragma once

#ifndef _Zprops
#define _Zprops


using namespace std;

#include "OgreString.h"
#include "Zobject.h"

class Zprops : public Zobject
{
public:
	~Zprops(void);
	Zprops();
	Zprops(string str, bool collide);
	Zprops(string str, string nodeName, Ogre::Vector3(position), Ogre::Vector3(scale), bool collide);
	Zprops(string str, string nodeName, Ogre::Vector3(position), int degree, bool collide);
	Zprops(string mesh, string entityName, string nodeName, bool collide);

	void update();

	Ogre::SceneNode* zObjectNode;


protected:

	Ogre::Camera* playerCam;
	Ogre::SceneNode* playerNode;

	// for player collision
	Ogre::AxisAlignedBox boudingBox;
	Ogre::Vector3 previousCamPos;

	int unit;
	bool canCollide;

};
#endif
