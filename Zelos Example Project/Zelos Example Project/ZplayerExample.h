#pragma once

#include "BaseApplication.h"
#include "OgreFrameListener.h"

class ZplayerExample : public BaseApplication
{
public :
	ZplayerExample(void);
	~ZplayerExample(void);
	bool frameStarted(Ogre::FrameEvent &evt);
	bool frameEnded(Ogre::FrameEvent &evt);

};