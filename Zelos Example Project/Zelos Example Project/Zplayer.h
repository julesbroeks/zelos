#pragma once

#ifndef _Zplayer
#define _Zplayer


using namespace std;

#include "OgreString.h"
#include "Zobject.h"


class Zplayer : public Zobject
{
public:
	~Zplayer(void);
	Zplayer();
	Zplayer(string playerNodeName );
	void update();
	Ogre::SceneNode* ZobjectNode;
	void ZkeyPressed(const OIS::KeyEvent &arg);
	void ZkeyReleased(const OIS::KeyEvent &arg);
	void ZmouseMoved(const OIS::MouseEvent &arg);
	bool moveForward = false;
	bool moveBackward = false;
	bool moveLeft = false;
	bool moveRight = false;
	bool jumping = false;
	bool fallingAterJump = false;
	float speed = 8.0f;
	float normalSpeed = 8.0f;
	float playerGravity = 0.5f;
	float playerHeight = 60.0f;
	float jumpHeight = 100.0f;
	float jumpSpeed = 5.0f;
	bool running = false;
	float stamina = 100.0f;
	Ogre::Quaternion cameraRotation;
	Ogre::Camera* zCamera;
	Zmain* zMain;



protected:
	

};
#endif
