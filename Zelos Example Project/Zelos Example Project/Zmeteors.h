#pragma once

#ifndef _Zmeteors
#define _Zmeteors
#include "ZobstacleEvent.h"
#include <OGRE/OgreSceneNode.h>
#include "Zrigidbody.h"
#include <time.h>
using namespace std;

class Zmeteors : public ZobstacleEvent
{



public:

	~Zmeteors(void);
	Zmeteors();

	void playSound();
	void update();
	void stopSound();
	void createParamsPanel(string panelText);
	void hideParamsPanel();
	void resetMeteors();
	list<Zrigidbody*>* meteorList;
	void startEvent();
	clock_t timeLastShow;

	ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
	SoundClass meteorSound;
};
#endif
