#pragma once

#ifndef _Zlevel
#define _Zlevel
#include "ZobstacleSandstorm.h"

using namespace std;

#include "Zobject.h"
#include "Zmain.h"
#include "ZlevelManager.h"
#include <time.h>
#include "ZobstacleEvent.h"	
#include "ZsoundSystemClass.h"

class Zlevel
{

public:
	~Zlevel(void);
	Zlevel();

	virtual void startLevel();
	virtual void stopLevel();
	virtual void fireObstacleEvent();
	virtual void createObjectivePanel(string panelText);
	virtual void hideObjectivePanel();
	virtual void createInventoryPanel(string panelText);
	virtual void hideInventoryPanel();
	virtual void createOxygenPanel(string panelText);
	virtual void hideOxygenPanel();
	virtual void createHealthPanel(string panelText);
	virtual void hideHealthPanel();
	virtual void showDeathOverlay();
	virtual void update();
	ZobstacleEvent* meteorRain;
	//ZobstacleEvent* sandStorm;

	string zOxygenCount;
	SoundClass breathing;
	SoundClass choking;
	SoundClass lastBreath;
	bool startedBreathing = false;
	bool startedChoking = false;

private:

	clock_t currentTime;
	clock_t lastOxygenUpdate;

	Ogre::Rectangle2D* rect;
};
#endif
