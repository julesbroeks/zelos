/**
*    @class		Zprops
*    @author	Galit Rahim
*    @date		11/4/2016
*    @brief		Will contain the props functions and update.
*
*    @section Description
*
*/

#include "Zprops.h"
#include "ZsoundSystemClass.h"
#include "ZlevelManager.h"

//---------------------------------------------------------------------------
/// Init for the player
Zprops::Zprops()
{

};

/// Init props with a mesh attached to the node
Zprops::Zprops(string str, bool collide)
{

	zObjectNode = Zobject::ZcreateObject(str);
	playerNode = zSceneManager->getSceneNode("PlayerNode");

	zObjectNode->showBoundingBox(false);

	canCollide = collide;
};

Zprops::Zprops(string str, string nodeName, Ogre::Vector3(position), Ogre::Vector3(scale), bool collide)
{

	zObjectNode = Zobject::ZcreateObject(str, nodeName, nodeName);

	zObjectNode->setPosition(position);
	zObjectNode->setScale(scale);

	playerNode = zSceneManager->getSceneNode("PlayerNode");

	zObjectNode->showBoundingBox(false);

	canCollide = collide;
};

Zprops::Zprops(string str, string nodeName, Ogre::Vector3(position), int degree, bool collide)
{
	zObjectNode = Zobject::ZcreateObject(str, nodeName, nodeName);

	zObjectNode->setPosition(position);
	zObjectNode->rotate(Ogre::Vector3::UNIT_X, Ogre::Radian(Ogre::Math::DegreesToRadians(degree)));

	playerNode = zSceneManager->getSceneNode("PlayerNode");

	zObjectNode->showBoundingBox(false);

	canCollide = collide;
};

/// Init props with a node name included
Zprops::Zprops(string mesh, string entityName, string nodeName, bool collide)
{

	zObjectNode = Zobject::ZcreateObject(mesh, entityName, nodeName);
	playerNode = zSceneManager->getSceneNode("PlayerNode");
	zObjectNode->showBoundingBox(false);

	canCollide = collide;

};

/// Deconstructor 
Zprops::~Zprops(void)
{

};

void Zprops::update()
{
		// Collide with player

		Ogre::AxisAlignedBox aab = zObjectNode->_getWorldAABB().intersection(playerNode->_getWorldAABB());

		if (canCollide) {

			if (!aab.isNull())
			{
				Ogre::Vector3 diff = playerNode->getPosition() - zObjectNode->getPosition();
				Ogre::Vector3 dir = diff.normalisedCopy();
				Ogre::Vector3 penentration = aab.getMaximum() - aab.getMinimum();
				Ogre::Vector3 trans = dir * Ogre::Math::Abs(penentration.normalisedCopy().dotProduct(dir)) * penentration.length() * 0.4f;

				playerNode->setPosition(playerNode->getPosition() + trans);

			}
		}
};


