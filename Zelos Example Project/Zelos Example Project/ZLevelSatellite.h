#pragma once

#ifndef _ZlevelSatellite
#define _ZlevelSatellite
#include "Zlevel.h"
#include "Zobject.h"
#include <time.h>

using namespace std;

class ZlevelSatellite : public Zlevel, public Zobject
{



public:

	~ZlevelSatellite(void);
	ZlevelSatellite();

	void startLevel();
	void stopLevel();
	void fireObstacleEvent();
	void createObjectivePanel(string panelText);
	void hideObjectivePanel();
	void update();

private:
	bool dishActive = false;
	clock_t lastDishUpdate;

	Ogre::Rectangle2D* rect;

};
#endif
