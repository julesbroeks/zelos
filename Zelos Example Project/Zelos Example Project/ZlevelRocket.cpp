/**
*    @class		ZlevelRocket
*    @author	Thomas Schoutsen
*    @date		10/4/2016
*    @brief		Create the get keycard objective level
*
*    @section Description
*
*/

#include "ZlevelRocket.h"
#include "Zprops.h"
#include "ZlevelManager.h"
#include "ZsoundSystemClass.h"

//---------------------------------------------------------------------------

ZlevelRocket::ZlevelRocket()
{
	// first get the rocket object in the scene
	zpRocket = Zobject::ZcreateObject("Lander.mesh", "EntityRocket", "NodeRocket");
	zpRocket->setPosition(1000, 130, 4500);
	zpRocket->setScale(70.0f, 70.0f, 70.0f);
	zpRocket->setVisible(false);
};

/// Deconstructor 
ZlevelRocket::~ZlevelRocket(void)
{

};


void ZlevelRocket::startLevel()
{
	OutputDebugString("*** Level Rocket Started ***\n");
	createObjectivePanel("Go to the rocket.");

	ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
	SoundClass rocketSound;
	zSoundManager->createSound(&rocketSound, "\\narrator\\GetToTheRocket.wav");
	zSoundManager->playSound(rocketSound, false);

	zpRocket->setVisible(true);
	inRocket = false;
}

void ZlevelRocket::stopLevel()
{
	Zlevel::stopLevel();

}

void ZlevelRocket::fireObstacleEvent()
{
	Zlevel::fireObstacleEvent();
}

void ZlevelRocket::createObjectivePanel(string panelText)
{
	Zlevel::createObjectivePanel(panelText);
}

void ZlevelRocket::hideObjectivePanel()
{
	Zlevel::hideObjectivePanel();
}

void ZlevelRocket::update()
{
	Zlevel::update();

	OutputDebugString("*** Level Rocket Updating ***\n");

	if (ZlevelManager::getZlevelManager()->zpIndicator->getEntity(ZlevelManager::getZlevelManager()->zpIndicator->zObjectNode)->isVisible())
	{
		ZlevelManager::getZlevelManager()->zpIndicator->updateIndicator(zpRocket->getPosition());
	}

	if (zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(zpRocket->getPosition()) < 5000)
	{
		createObjectivePanel("You can escape the planet!");
	}

	if (zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(zpRocket->getPosition()) < 1000 && inRocket == false)
	{
		//createObjectivePanel("You have completed the game! Thanks for playing");

		// Create background material
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Win", "General");
		material->getTechnique(0)->getPass(0)->createTextureUnitState("ship.jpg");
		material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
		material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
		material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

		// Create background rectangle covering the whole screen
		rect = new Ogre::Rectangle2D(true);
		rect->setCorners(-1.0, 1.0, 1.0, -1.0);
		rect->setMaterial("Win");

		// Use identity view/projection matrices
		rect->setUseIdentityProjection(true);
		rect->setUseIdentityView(true);

		// Use infinite AAB to always stay visible
		Ogre::AxisAlignedBox aabInf;
		aabInf.setInfinite();
		rect->setBoundingBox(aabInf);

		// Render just before overlays
		rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY - 1);

		// Attach to scene
		zSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(rect);

		Zmain::getZmain()->playerInBase = true;
		Zmain::getZmain()->meteorsPaused = true;

		Zmain::getZmain()->zTrayManager->hideAll();

		inRocket = true;
	}
}