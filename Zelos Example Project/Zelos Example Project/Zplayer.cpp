/**
*    @class		Zplayer
*    @author	Jules Broeks
*    @date		12/4/2016
*    @brief		Will contain the player functions and update.
*
*    @section Description
*
*/

#include "Zplayer.h"


//---------------------------------------------------------------------------
/// Init for the player
Zplayer::Zplayer()
{

};
/// Init player with a mesh attached to the node
Zplayer::Zplayer(string playerNodeName)
{
	zSceneManager = Zmain::getZmain()->zSceneManager;
	zCamera = zSceneManager->getCamera("PlayerCam");
	Ogre::Entity* ogreEntity = zSceneManager->createEntity("cube.mesh"); // placeholder cube as player, used for collision
	ZobjectNode = zSceneManager->getRootSceneNode()->createChildSceneNode(playerNodeName);
	ZobjectNode->attachObject(ogreEntity);
	ZobjectNode->showBoundingBox(false);
	ZobjectNode->setVisible(false);

	ZobjectNode->setPosition(0, 150, 0);


};

/// Deconstructor 
Zplayer::~Zplayer(void)
{

};

void Zplayer::update()
{
	Ogre::Vector3 currentPosition = ZobjectNode->getPosition();
	Ogre::Vector3 newPosition = Ogre::Vector3(currentPosition.x, currentPosition.y, currentPosition.z);

	
	if (running && stamina > 0)
	{
		speed = normalSpeed*2;
		stamina -= 0.5f;
	}
	else
	{
		speed = normalSpeed;
		stamina += 0.1f;
	}


	if (moveForward)
	{
		newPosition.z -= speed;
		
	}
	if (moveBackward)
	{
		newPosition.z += speed;
	}
	if (moveLeft)
	{
		newPosition.x -= speed;
	}
	if (moveRight)
	{
		newPosition.x += speed;
	}
	// check if player is on the ground else apply gravity. if player is in below terrain it moves the player up. + jump method
	
	Ogre::Ray collisionRay(Ogre::Vector3(currentPosition.x, 5000.0f, currentPosition.z ), Ogre::Vector3::NEGATIVE_UNIT_Y);
	Ogre::TerrainGroup::RayResult result =  Zmain::getZmain()->zTerrainGroup->rayIntersects(collisionRay);
	if (result.terrain) 
	{
		Ogre::Real terrainHeight = result.position.y;
		if (jumping)
		{
			if (currentPosition.y >= (terrainHeight + playerHeight + jumpHeight))
			{
				jumping = false;
				fallingAterJump = true;
			}
			else
			{
				newPosition.y = newPosition.y + jumpSpeed;
			
			
			}
		}
		else if (fallingAterJump)
		{
			if (currentPosition.y <= (terrainHeight + playerHeight + 1.0f))
			{
				fallingAterJump = false;
			}
			else
			{
				newPosition.y = newPosition.y - jumpSpeed;
	

			}
		}
		else
		{
			// When the player bottom is below the terrain, place the camera above the terrain
			if (currentPosition.y < (terrainHeight + playerHeight)) {
				newPosition.y = terrainHeight + playerHeight;

			}
			// When the camera is above the height + the max height the player can be it will be placed back // the 5 is extra so there is a safezone where nothing is done ( else you get up and down flickering )
			else if (currentPosition.y >(terrainHeight + playerHeight + 1.0f)) {
				newPosition.y = terrainHeight + 1.0f + playerHeight;
		
			}
		}
	}


	Ogre::Vector3 targetPosition = (newPosition - currentPosition);
	float tempY = targetPosition.y;
	targetPosition = zCamera->getOrientation() * targetPosition;
	targetPosition.y = tempY;
	ZobjectNode->translate(targetPosition, Ogre::Node::TS_LOCAL);
	
	//ZobjectNode->setPosition(newPosition);
	zCamera->setPosition(ZobjectNode->getPosition());
};


void Zplayer::ZkeyPressed(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_W)   // spawn meteor
	{
		moveForward = true;
	}
	if (arg.key == OIS::KC_A)   // spawn meteor
	{
		moveLeft = true;
	}
	if (arg.key == OIS::KC_D)   // spawn meteor
	{
		moveRight = true;
	}
	if (arg.key == OIS::KC_S)   // spawn meteor
	{
		moveBackward = true;
	}
	if (arg.key == OIS::KC_SPACE)   // spawn meteor
	{
		jumping = true;
	}
	if (arg.key == OIS::KC_LSHIFT)   // spawn meteor
	{
		running = true;
	}
 
}
//---------------------------------------------------------------------------
void Zplayer::ZkeyReleased(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_W)  
	{
		moveForward = false;
	}
	if (arg.key == OIS::KC_A) 
	{
		moveLeft = false;
	}
	if (arg.key == OIS::KC_D)  
	{
		moveRight = false;
	}
	if (arg.key == OIS::KC_S)  
	{
		moveBackward = false;
	}
	if (arg.key == OIS::KC_LSHIFT)   
	{
		running = false;
	}

}
void Zplayer::ZmouseMoved(const OIS::MouseEvent &arg)
{
	zCamera->yaw(Ogre::Degree(arg.state.X.rel * -0.1f));
	zCamera->pitch(Ogre::Degree(arg.state.Y.rel * -0.1f));

	if (zCamera->getOrientation().getPitch().valueDegrees() > 90)
	{
		// value of camera is over 90 degrees up so set back the rotation.
		zCamera->pitch(Ogre::Degree(-arg.state.Y.rel * -0.1f));
	}
	if (zCamera->getOrientation().getPitch().valueDegrees() < -90)
	{
		// value of camera is under -90 degrees up so set back the rotation.
		zCamera->pitch(Ogre::Degree(-arg.state.Y.rel * -0.1f));
	}


}


