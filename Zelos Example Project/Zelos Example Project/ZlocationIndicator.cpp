#include "ZlocationIndicator.h"

ZlocationIndicator::ZlocationIndicator()
{
	
};

ZlocationIndicator::ZlocationIndicator(string mesh)
{
	zObjectNode = Zobject::ZcreateObject(mesh);
	zObjectNode->setScale(Ogre::Vector3(0.2f, 0.2f, 0.2f));
	hideIndicator();
}

void ZlocationIndicator::updateIndicator(Ogre::Vector3 lookAtPosition)
{
	
	Ogre::Vector3 camPos = Zobject::zSceneManager->getCamera("PlayerCam")->getPosition();
	Ogre::Vector3 camDir = zSceneManager->getCamera("PlayerCam")->getDirection();

	zObjectNode->setPosition(camPos + (camDir * (-100, 100, 150)));

	zObjectNode->lookAt(lookAtPosition, Ogre::Node::TS_WORLD, Ogre::Vector3::UNIT_Y);

	int xOffset = zSceneManager->getCamera("PlayerCam")->getViewport()->getActualHeight() * 0.08f ;
	int yOffset = zSceneManager->getCamera("PlayerCam")->getViewport()->getActualWidth() * 0.05f;

	zObjectNode->translate(zSceneManager->getCamera("PlayerCam")->getOrientation() * Ogre::Vector3(xOffset, yOffset, 0));

}

void ZlocationIndicator::showIndicator()
{
	zObjectNode->setVisible(true);
}

void ZlocationIndicator::hideIndicator()
{
	zObjectNode->setVisible(false);
}
