/**
*    @class		ZlevelKeycard
*    @author	Thomas Schoutsen, Galit Rahim
*    @date		10/4/2016
*    @brief		Create the get keycard objective level
*
*    @section Description
*
*/

#include "ZlevelKeycard.h"
#include "ZlevelManager.h"
#include "Zmain.h"
#include "ZsoundSystemClass.h"

//---------------------------------------------------------------------------


ZlevelKeycard::ZlevelKeycard()
{
	// first spawn the keycard somewhere on the map
	zpKeycard = Zobject::ZcreateObject("keycard.mesh", "entityKeycard", "nodeKeycard");
	zpKeycard->setPosition(Ogre::Vector3(-5000, 350, -5000));
	zpKeycard->setScale(Ogre::Vector3(20, 20, 20));
	zpKeycard->setVisible(false);

};

/// Deconstructor 
ZlevelKeycard::~ZlevelKeycard(void)
{

};


void ZlevelKeycard::startLevel()
{
	OutputDebugString("*** Level Keycard Started ***\n");

	zpKeycard->setVisible(true);
	createObjectivePanel("There must be a keycard somewhere around here that could provide access to the base");

	ZsoundSystemClass *zSoundManager = new ZsoundSystemClass();
	SoundClass findKeycardSound;
	zSoundManager->createSound(&findKeycardSound, "\\narrator\\FindKeycard.wav");
	zSoundManager->playSound(findKeycardSound, false);

	Ogre::StringVector pickupItemText;
	pickupItemText.push_back("Press 'E' to access base");
	zBasePanel = zTrayManager->createParamsPanel(OgreBites::TL_RIGHT, "basePanel", 275, pickupItemText);
	zBasePanel->hide();
	zTrayManager->removeWidgetFromTray(zBasePanel);

	inBase = 1;

}

void ZlevelKeycard::stopLevel()
{
	
	Zlevel::stopLevel();
	OutputDebugString("*** Keycard Used! ***\n");
	OutputDebugString("*** Level Complete! ***\n");

	
}

void ZlevelKeycard::fireObstacleEvent()
{
	Zlevel::fireObstacleEvent();
}

void ZlevelKeycard::createObjectivePanel(string panelText)
{
	Zlevel::createObjectivePanel(panelText);
}

void ZlevelKeycard::hideObjectivePanel()
{
	Zlevel::hideObjectivePanel();
}
void ZlevelKeycard::createInventoryPanel(string panelText)
{

	Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("TextureMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	material->getTechnique(0)->getPass(0)->createTextureUnitState("keycard.jpg");

	// Create an overlay
	overlay = overlayManager.create("OverlayPanel");

	// Create a panel
	Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*> (overlayManager.createOverlayElement("Panel", "myPanel"));
	panel->setPosition(0.81, 0.045);
	panel->setDimensions(0.15, 0.1);
	panel->setMaterialName("TextureMaterial");
	// Add the panel to the overlay
	overlay->add2D(panel);

	// Show the overlay
	overlay->show();
}

void ZlevelKeycard::hideInventoryPanel()
{
	overlay->hide();
}


/// Is called each frame
void ZlevelKeycard::update()
{
	Zlevel::update();

	/*OutputDebugString("*** Level Keycard Updating ***\n");*/

	// Check if player has keycard
	if (!hasKeycard)
	{
		// Point indicator towards keycard object
		if (ZlevelManager::getZlevelManager()->zpIndicator->getEntity(ZlevelManager::getZlevelManager()->zpIndicator->zObjectNode)->isVisible())
		{
			ZlevelManager::getZlevelManager()->zpIndicator->updateIndicator(zpKeycard->getPosition());
		}

		// Show keycard feedback when keycard is picked up
		if (zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(zpKeycard->getPosition()) < 150 )
		{
			zpKeycard->setVisible(false);
			createInventoryPanel("Keycard");
			hasKeycard = true;

			OutputDebugString("*** Found Keycard! ***\n");
		}
	}
	else
	{

		// Check if player is near base to return keycard
		if (ZlevelManager::getZlevelManager()->zpIndicator->getEntity(ZlevelManager::getZlevelManager()->zpIndicator->zObjectNode)->isVisible())
		{
			ZlevelManager::getZlevelManager()->zpIndicator->updateIndicator( Zmain::getZmain()->zSceneManager->getSceneNode("zHabitatNode")->getPosition() );
		}

		if (zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(Zmain::getZmain()->zSceneManager->getSceneNode("zHabitatNode")->getPosition()) < 1000 && inBase == 1)
		{
			zTrayManager->moveWidgetToTray(zBasePanel, OgreBites::TL_RIGHT, 0);
			zBasePanel->show();
			hideObjectivePanel();

			if (GetAsyncKeyState('E') & 0x8000)
			{
				// Create background material
				Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
				material->getTechnique(0)->getPass(0)->createTextureUnitState("base.jpg");
				material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
				material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
				material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

				// Create background rectangle covering the whole screen
				rect = new Ogre::Rectangle2D(true);
				rect->setCorners(-1.0, 1.0, 1.0, -1.0);
				rect->setMaterial("Background");

				// Use identity view/projection matrices
				rect->setUseIdentityProjection(true);
				rect->setUseIdentityView(true);

				// Use infinite AAB to always stay visible
				Ogre::AxisAlignedBox aabInf;
				aabInf.setInfinite();
				rect->setBoundingBox(aabInf);

				// Render just before overlays
				rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY - 1);

				// Attach to scene
				zSceneManager->getRootSceneNode()->createChildSceneNode()->attachObject(rect);

				//clock
				lastKeycardUpdate = clock();

				zBasePanel->hide();
				zTrayManager->removeWidgetFromTray(zBasePanel);
				
				inBase = 2;
				hideInventoryPanel();
				
				Zmain::getZmain()->playerInBase = true;
				Zmain::getZmain()->meteorsPaused = true;
			}
		}
		else if (zSceneManager->getSceneNode("PlayerNode")->getPosition().distance(Zmain::getZmain()->zSceneManager->getSceneNode("zHabitatNode")->getPosition()) > 1000 )
		{
			createObjectivePanel("Go to the base");
		}
		else
		{
			clock_t currentTime = clock();
			clock_t differenceInTicks = currentTime - lastKeycardUpdate;
			clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);

			OutputDebugString("*** Delay ***\n");

			if (differenceInMs >= 7000 && inBase == 2)
			{
				OutputDebugString("*** Delay over, stopping current Level ***\n");
				Zmain::getZmain()->playerInBase = false;
				Zmain::getZmain()->meteorsPaused = false;
				ZlevelManager::getZlevelManager()->zOxygenInt += 20;
				
				if (ZlevelManager::getZlevelManager()->zHealthInt < 100)
				{
					ZlevelManager::getZlevelManager()->zHealthInt += 25;
				}

				delete rect;
				stopLevel();

			}
		}
	}
};