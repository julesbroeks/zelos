/**
*    @class		Zcamera
*    @author	Jules Broeks
*    @date		8/4/2016
*    @brief		Will contain the player functions and update.
*
*    @section Description
*
*/

#include "Zcamera.h"


//---------------------------------------------------------------------------
/// Init for the player
Zcamera::Zcamera()
{

};
/// Init player with a mesh attached to the node
Zcamera::Zcamera(Ogre::Camera* cam)
{
	zCamera = cam;
};


/// Deconstructor 
Zcamera::~Zcamera(void)
{

};

void Zcamera::update()
{

	float pitchAngle = 10.0f;
	float pitchAngleSign = 0;

	pitchAngle = zCamera->getOrientation().getPitch().valueDegrees();
	
	

};


