#pragma once

#ifndef _Ztimer
#define _Ztimer


using namespace std;

#include "OgreString.h"
#include "Zobject.h"

class Ztimer : public Zobject
{
	int millisecond;
	Ogre::Timer* theTimer;
	Ogre::Real countDown;
public:
	~Ztimer(void);
	Ztimer();
	Ztimer(int durationInMinutes);
	bool update();
	void addMinutes(int minutes);
	string getValue();
};
#endif
