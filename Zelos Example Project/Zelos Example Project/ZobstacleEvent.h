#pragma once

#ifndef _ZobstacleEvent
#define _ZobstacleEvent
#include <string>

using namespace std;


class ZobstacleEvent
{

public:
	~ZobstacleEvent(void);
	ZobstacleEvent();

	virtual void playSound();
	virtual void update();
	virtual void stopSound();
	virtual void createParamsPanel(string panelText);
	virtual void hideParamsPanel();
	virtual void startEvent();

private:



};
#endif