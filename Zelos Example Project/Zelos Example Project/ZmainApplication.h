
#pragma once
/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

using namespace std;

#ifndef __ZmainApplication_h_
#define __ZmainApplication_h_

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include "BaseApplication.h"
#include "Zmain.h"



//---------------------------------------------------------------------------

class ZmainApplication : public BaseApplication
{
public:
	ZmainApplication(void);
	virtual ~ZmainApplication(void);
	class Zmain* zMain;
	 list<Zmain> ZmainList;
	 OgreBites::SdkTrayManager* zTrayMgr;
	 OgreBites::ParamsPanel* zTimerPanel;
	 class Ztimer *theTimer;

protected:
    virtual void createScene(void);
	virtual void destroyScene();
	virtual void createFrameListener(void);
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);


	virtual bool keyPressed(const OIS::KeyEvent &arg);
	virtual bool keyReleased(const OIS::KeyEvent &arg);
	virtual bool mouseMoved(const OIS::MouseEvent &arg);

private:

};

//---------------------------------------------------------------------------

#endif // #ifndef __ZmainApplication_h_

//---------------------------------------------------------------------------
