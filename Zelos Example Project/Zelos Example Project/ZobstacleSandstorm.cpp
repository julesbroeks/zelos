#include "ZobstacleSandstorm.h"
#include "OgreMeshManager.h"
#include "Zmain.h"
#include "Zrigidbody.h"
#include "ZsoundSystemClass.h"

ZobstacleSandstorm::ZobstacleSandstorm()
{
}


ZobstacleSandstorm::~ZobstacleSandstorm()
{

}


void ZobstacleSandstorm::startEvent()
{
	ZobstacleEvent::startEvent();


	OutputDebugString("*** Stormevent started ***\n");
	createParamsPanel("Sandstorm incoming!");

	// play sound
	playSound();
}

void ZobstacleSandstorm::stopEvent()
{

}

void ZobstacleSandstorm::playSound()
{
	zSoundManager->createSound(&stormSound, "\\wind.mp3");
	zSoundManager->playSound(stormSound, false);
}

void ZobstacleSandstorm::stopSound()
{
	zSoundManager->releaseSound(stormSound);
}

void ZobstacleSandstorm::createParamsPanel(string panelText)
{
	ZobstacleEvent::createParamsPanel(panelText);
}

void ZobstacleSandstorm::hideParamsPanel()
{
	ZobstacleEvent::hideParamsPanel();
}

void ZobstacleSandstorm::update()
{

	if (stormActive)
	{
		if (!ZlevelManager::getZlevelManager()->damageFogIsDisplayed){
			if (isIncreasing) // increase fog
			{
				stormPower += 0.00002f;

				if (stormPower >= maxStormPower)
				{
					OutputDebugString("*** reached max ***\n");
					isIncreasing = false;
					hideParamsPanel();
					lastUpdate = clock();
				}
			}
			else // decrease fog
			{
				clock_t currentTime = clock();

				clock_t differenceInTicks = currentTime - lastUpdate;
				clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
				if (differenceInMs >= 15000)
				{

					stormPower -= 0.00002f;
					if (stormPower <= minStormPower)
					{
						isIncreasing = true;
						stormActive = false;
						// stop sound
						stopSound();
						OutputDebugString("*** Storm stopped ***\n");
						lastUpdate = clock();
					}


				}


			}

			Zmain::getZmain()->zSceneManager->setFog(Ogre::FOG_EXP, fadeColour, stormPower);
		}
	} else
	{
		
		// storm paused
		clock_t currentTime = clock();

		clock_t differenceInTicks = currentTime - lastUpdate;
		clock_t differenceInMs = differenceInTicks / (CLOCKS_PER_SEC / 1000);
		if (differenceInMs >= 40000)
		{
			startEvent();
			stormActive = true;
			lastUpdate = clock();
		}


	}

	
}